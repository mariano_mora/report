\documentclass[nouppercase]{report}
\usepackage{authblk}
\usepackage{paralist,graphicx}

\title{Semantic research on music and humanities: a report}

\author[1]{Mariano Mora-McGinity}
\author[2]{Gary Ogilvie}
\author[1]{Gy\"{o}rgy Fazekas}
\author[1]{Simon Dixon}
\author[2]{Jasmine Shadrack}
\affil[1]{Queen Mary University, London}
\affil[2]{University of Northampton, Northampton}
\renewcommand\Authands{ and }




\begin{document}

\maketitle

% outline:
%
% - original purpose
% - what we have done and why
% 
% implementation
%
% 	- research implementation: survey, report on research interests and user patterns (Gary?)
%
% 	- technical implementation (Mariano?)
% 		- semantic DB
% 		- web crawl and web service
% 		- interface and faceted browsing



\section{Technical implementation}

Our project has been implemented with the goal in mind of offering researchers a novel way of browsing music related data. In order to do this we have searched for data, looked for ways to connect pieces of data together and presented it in a way which will grant the user easy access to research resources, but also hopefully allow the researcher to discover new and related areas of resources which might be of interest and of which she was hitherto unaware. These three tasks have been addressed, respectively, by:
\begin{enumerate}
	\item A web searching application. 
	\item A semantic database.
	\item A web interface application.
\end{enumerate}
In the next sections we will discuss each of these elements in more detail.

\subsection{Data searching application}

Our main method of gathering data has been to query several services available on the web. Many web resources offer API's and services to query their data.\footnote{A complete listing of web resources on music can be found at: \\
  http://www.programmableweb.com/category/all/apis?keyword=music}
There are also a limited number of research sites that offer rest services, mainly \textit{mendeley}\footnote{http://apidocs.mendeley.com/home/public-resources} and \textit{core}\footnote{http://core.kmi.open.ac.uk/search}.
We begin with a set of well-known artists. Some services, such as \textit{echonest}\footnote{http://the.echonest.com/} use algorithms to calculate similarity between artists according to criteria such as style, fans with similar interests or presence in the media, etc. We used that information to connect artists to other artists and try to find research resources dealing with them.\\
The main source of data used in our project has been information about articles related to artists in our database. The data offered by services such as \textit{mendeley} includes:
\begin{itemize}
   \item
   	Title
   \item
    Type of publication: article, proceedings, book...
  \item 
    List of authors
  \item
    Abstract
  \item
    Publication (journal) in which the article appears
  \item
    Related articles
  \item 
    Disciplines
\end{itemize}

\subsection{Data Linking and semantic database}
Our semantic database links artists with research related to them. The two main ontologies used to bind these two domains together are the \emph{Music Ontology}\footnote{http://musicontology.com/} and the bibliographic ontology \emph{fabio}\footnote{http://www.essepuntato.it/lode/http://purl.org/spar/fabio}. Graphs are built based on the artist. A first binding is established between each artist and all similar (according to the web resources) artists. A second level of binding is then established between each artist and all the articles available in our semantic database that are related to the artist. From each article the researcher can access information about related articles, other articles from the same author, articles published in the same journal and articles that belong to the same disciplines, or are tagged in a similar way.   
Many of the classes and predicates can be found in the ontologies used. We created several classes and predicates to help bind relations.  Examples of these are:
\begin{compactitem}
  \item
    isAbout
  \item 
    isPublishedIn
  \item
    isSimilarTo
  \item
    isRelatedTo
  \item
    isSimilarlyTaggedAs
\end{compactitem}
Most of the predicates and classes used have been motivated by the desire to offer the user different approaches to navigating the data. The navigation itself has also been determined by the principle of presenting the data to the researcher in a clear and coherent way.

\section{Web application interface}
\label{ss:results}

Our project is aimed at researchers: our goal is to provide an interface for them which will allow them to research better, faster and in ways which they might not have thought of.
The main motivation while designing the data visualization interface is to facilitate the researcher's navigation through the data. There are many conceivable ways to enter a connected graph. Our intention has been to offer the researcher different ways to begin the search and different possibilities to continue at each step of the navigation. For this reason we decided on a \emph{faceted browsing} approach. We felt other alternatives such as network visualizations do not make explicit a categorization of relationships which we considered important for researchers. Hence the decision to present data in tree format. Figure~\ref{fg:screenshot} shows a tree as may be seen by a researcher. The tree's branches represent different \emph{facets} or taxonomies for browsing new data. An instance of a branch is, for instance, a new sub-tree of journals that publish articles about that artist. Opening that node uncovers new branches of disciplines included in that particular journal, as well as all nodes to all articles published in that journal, as well as all authors who have published in that journal. The user reaches eventually a leaf with no further facets related to the original root. This leaf serves as the root of a new tree, and the user finds herself in a new domain from which to continue browsing. Such happens, for instance, when the user reaches a node containing a discipline, a keyword or a tag. At other times, a leaf will direct to an external resource, such as the website of an article or a journal.
A right-hand side panel displays important article data including publication data, discipline, author, abstract and where possible a web link to the original text. Users have the appropriate amount of information to discover whether the article is appropriate to their research and to locate the article should a link be unavailable. Centralization of relevant resources is the key element in this data model.
Long names, such as article or journal titles are abbreviated in the tree. This can be difficult at first glance to identify an article that suits a particular research area. The user must click on each article to see the full title in the right panel. Also, it is currently not possible to search by keyword and therefore all articles relating to an artist, or discipline, are shown. Implementation of this is crucial to displaying not only articles relevant to the artist, but those also relevant to the research area.


\begin{figure}
  \centering
  \includegraphics[width=12cm,height=9cm]{semantic_image.png}
  \caption{View of the data visualization interface}
  \label{fg:screenshot}
\end{figure}


The interface is the final representation of the whole process of data acquisition and binding. Acquiring data through web-services offers many advantages, but also certain restrictions. Web-services offer a clean, modular and safe entrance to web resources. However, they also greatly limit the access to the data itself: queries can only be carried out through the API offered by the service. The API is also responsible for parsing the query, and most API will try to be restrictive rather than lenient. The alternative is, of course, to allow queries which might cause the server to collapse, so the decision to impose constrictions is perfectly understandable. However, it limits the client's access to data. We might, for instance, like to query directly for more than one term, or build views of our results, according to how we want to structure our data. 
Our approach has been to access many small pieces of data from as many resources as possible and bind them together using coherent relations.



\section{Future work}

Future work should focus on developing intelligent techniques to provide humanities researchers with more detailed relationships of areas of data. Our project has been heading in a direction characterised by the following traits:
\begin{itemize}
  \item A database of knowledge about artists. Such a database would allow us to identify and discriminate articles, newspaper pieces, blog entries and websites related to the artist. A machine learning approach to developing this database would ensure that, as the volume of data about an artist that we accumulate increases, we are able to acquire more new and pertinent data.
  \item Natural language processing techniques which would allow us to extract content from articles and identify potential relationships between concepts contained in them. As an example: we have found that an article about an artist such as \emph{Lady Gaga} might contain references to sociological, psychological, cultural or gender related issues. Using algorithms to pick out concept representations in texts could allow us to link together resources which pertain to similar research areas.
  \item The faceted approach to browsing the data is, we feel, a valuable experience for the researcher. We intend to extend this approach using techniques made possible by a greater and more detailed volume of data. As of today, access to the browsing interface happens after the user has chosen an artist to start searching from. We think the user should be offered the possibility of browsing from any of the elements that the system has learned to recognise. This would enable searches such as, for instance: `'give me information on economics and fashion related to a genre of music", or `'how does a pop star influence young people's behavior". The system would have learned to classify articles that relate to every issue involved in these queries and could build a faceted tree based on them.
\end{itemize}




\end{document}